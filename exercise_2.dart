import 'package:exercise_2/exercise_2.dart' as exercise_2;


import 'dart:io';

void main(List<String> args) {
  String? input = stdin.readLineSync()!;
  List<String> words = input.toLowerCase().split(' ');
  
  var map = {};

  for (var word in words) {
    if (!map.containsKey(word)) {
      map[word] = 1;
    } else {
      map[word] += 1;
    }
  }

  print(map);
}